CREATE TABLE Users (
                       user_id BIGSERIAL PRIMARY KEY,
                       username VARCHAR(255) NOT NULL,
                       email VARCHAR(255),
                       phone VARCHAR(20),
                       created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE Events (
                        event_id SERIAL PRIMARY KEY,
                        user_id INT DEFAULT NULL,
                        event_type VARCHAR(100) NOT NULL,
                        event_date DATE NOT NULL,
                        guest_count INT NOT NULL,
                        status VARCHAR(50) NOT NULL,
                        created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                        FOREIGN KEY (user_id) REFERENCES Users(user_id)
);

CREATE TABLE Locations (
                           location_id SERIAL PRIMARY KEY,
                           name VARCHAR(255) NOT NULL,
                           address TEXT NOT NULL,
                           capacity INT NOT NULL,
                           type VARCHAR(100) NOT NULL
);

CREATE TABLE Bookings (
                          booking_id SERIAL PRIMARY KEY,
                          event_id INT NOT NULL,
                          location_id INT NOT NULL,
                          booking_date DATE NOT NULL,
                          status VARCHAR(50) NOT NULL,
                          FOREIGN KEY (event_id) REFERENCES Events(event_id),
                          FOREIGN KEY (location_id) REFERENCES Locations(location_id)
);

CREATE TABLE Catering (
                          catering_id SERIAL PRIMARY KEY,
                          event_id INT NOT NULL,
                          provider_name VARCHAR(255) NOT NULL,
                          menu_details TEXT NOT NULL,
                          price DECIMAL(10, 2) NOT NULL,
                          status VARCHAR(50) NOT NULL,
                          FOREIGN KEY (event_id) REFERENCES Events(event_id)
);

CREATE TABLE Tasks (
                       task_id SERIAL PRIMARY KEY,
                       event_id INT NOT NULL,
                       description TEXT NOT NULL,
                       status VARCHAR(50) NOT NULL,
                       FOREIGN KEY (event_id) REFERENCES Events(event_id)
);

CREATE TABLE UserState (
                           state_id SERIAL PRIMARY KEY,
                           user_id bigint NOT NULL,
                           current_command VARCHAR(100) NOT NULL,
                           data text,  -- Для хранения параметров, таких как дата, тип мероприятия и т.д.
                           last_update TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                           FOREIGN KEY (user_id) REFERENCES Users(user_id),
                           UNIQUE (user_id)  -- Добавлено уникальное ограничение для использования в ON CONFLICT
);

ALTER TABLE UserState
    ALTER COLUMN data TYPE jsonb USING data::jsonb;

CREATE TABLE Catering (
                          catering_id SERIAL PRIMARY KEY,
                          event_id INT NOT NULL,
                          provider_name VARCHAR(255) NOT NULL,
                          menu_details TEXT NOT NULL,
                          price DECIMAL(10, 2) NOT NULL,
                          status VARCHAR(50) NOT NULL,
                          FOREIGN KEY (event_id) REFERENCES Events(event_id)
);

INSERT INTO Events (event_type, event_date, guest_count, status)
VALUES
    ('Wedding', '2024-06-20', 100, 'Scheduled'),
    ( 'Conference', '2024-07-15', 250, 'Scheduled'),
    ( 'Birthday', '2024-08-30', 50, 'Scheduled'),
    ( 'Corporate', '2024-09-10', 300, 'Cancelled'),
    ( 'Meeting', '2024-10-05', 20, 'Scheduled'),
    ( 'Seminar', '2024-11-15', 80, 'Pending'),
    ( 'Gala', '2024-12-25', 200, 'Scheduled'),
    ( 'Reception', '2024-01-30', 150, 'Cancelled'),
    ( 'Workshop', '2024-02-20', 40, 'Scheduled'),
    ( 'Exhibition', '2024-03-15', 300, 'Pending');

INSERT INTO Catering (event_id, provider_name, menu_details, price, status)
VALUES
    (1, 'Provider A', 'Chicken, Fish, Vegetarian', ROUND((RANDOM() * 100 + 100)::numeric, 2), 'Active'),
    (2, 'Provider B', 'Beef, Vegan, Salad', ROUND((RANDOM() * 100 + 100)::numeric, 2), 'Pending'),
    (3, 'Provider C', 'Pasta, Pork, Rice', ROUND((RANDOM() * 100 + 100)::numeric, 2), 'Active'),
    (4, 'Provider D', 'Lamb, Buffet', ROUND((RANDOM() * 100 + 100)::numeric, 2), 'Cancelled'),
    (5, 'Provider E', 'Seafood, Desserts', ROUND((RANDOM() * 100 + 100)::numeric, 2), 'Pending'),
    (6, 'Provider F', 'BBQ, Hamburgers, Sides', ROUND((RANDOM() * 100 + 100)::numeric, 2), 'Active'),
    (7, 'Provider G', 'Sandwiches, Soups, Salads', ROUND((RANDOM() * 100 + 100)::numeric, 2), 'Cancelled'),
    (8, 'Provider H', 'Steak, Tacos, Beans', ROUND((RANDOM() * 100 + 100)::numeric, 2), 'Active'),
    (9, 'Provider I', 'Sushi, Tempura, Miso Soup', ROUND((RANDOM() * 100 + 100)::numeric, 2), 'Pending'),
    (10, 'Provider J', 'Breakfast, Eggs, Pancakes', ROUND((RANDOM() * 100 + 100)::numeric, 2), 'Active');

INSERT INTO Locations (name, address, capacity, type)
VALUES
    ('Location 1', '1234 Street, City', FLOOR(RANDOM() * 100 + 20)::INT, 'Conference'),
    ('Location 2', '2345 Avenue, City', FLOOR(RANDOM() * 200 + 50)::INT, 'Wedding'),
    ('Location 3', '3456 Boulevard, City', FLOOR(RANDOM() * 150 + 30)::INT, 'Corporate'),
    ('Location 4', '4567 Road, City', FLOOR(RANDOM() * 300 + 100)::INT, 'Private'),
    ('Location 5', '5678 Lane, City', FLOOR(RANDOM() * 400 + 150)::INT, 'Exhibition'),
    ('Location 6', '6789 Way, City', FLOOR(RANDOM() * 250 + 60)::INT, 'Meeting'),
    ('Location 7', '7890 Drive, City', FLOOR(RANDOM() * 180 + 70)::INT, 'Seminar'),
    ('Location 8', '8901 Park, City', FLOOR(RANDOM() * 350 + 90)::INT, 'Gala'),
    ('Location 9', '9012 Square, City', FLOOR(RANDOM() * 220 + 40)::INT, 'Reception'),
    ('Location 10', '10123 Place, City', FLOOR(RANDOM() * 160 + 80)::INT, 'Workshop');
