<?php

require_once __DIR__.'/src/Factory/HandlerFactory.php';
require_once __DIR__.'/src/CallbackCommands/CallbackqueryCommand.php';

use Events\Daniel\DB\DatabaseInterface;
use Events\Daniel\Repository\UserRepository;
use Events\Daniel\Repository\CateringRepository;
use Events\Daniel\Repository\UserStateRepository;
use Events\Daniel\Repository\EventTypeRepository;
use Events\Daniel\Repository\LocationRepository;
use function DI\create;

return [

    // Repositories instances
    'db_connection' => create(DatabaseInterface::class),
    'user_repository' => create(UserRepository::class),
    'catering_repository' => create(CateringRepository::class),
    'confirm_repository' => create(UserStateRepository::class),
    'event_type_repository' => create(EventTypeRepository::class),
    'location_repository' => create(LocationRepository::class)
];