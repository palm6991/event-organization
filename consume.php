<?php

require "vendor/autoload.php";

use PhpAmqpLib\Connection\AMQPStreamConnection;
use Longman\TelegramBot\Telegram;
use Events\Daniel\Consumer\ConfirmationHandlerConsumer;

$connection = new AMQPStreamConnection('localhost', 5672, 'my_user', 'T1y04lWk167MkyEK3YFk');
$telegram = new Telegram('6891871038:AAH4ZPG7MJjxWkck38J7KHitdMBuhA8IDyY', 'PalmSieBot');

$consumer = new ConfirmationHandlerConsumer($connection, $telegram);
$consumer->listen();