<?php

namespace Events\Daniel\Consumer;

use PhpAmqpLib\Connection\AMQPStreamConnection;
use Longman\TelegramBot\Telegram;
use Longman\TelegramBot\Request;

class ConfirmationHandlerConsumer
{
    private $connection;
    private $channel;
    private $queue = 'confirmations';
    private $telegram;

    public function __construct(AMQPStreamConnection $connection, Telegram $telegram)
    {
        $this->connection = $connection;
        $this->channel = $this->connection->channel();
        $this->channel->queue_declare($this->queue, false, true, false, false);
        $this->telegram = $telegram;
    }

    public function listen()
    {

        $callback = function ($msg) {
            $data = json_decode($msg->body, true);
            $chatId = $data['chat_id'] ?? null;
            if ($chatId) {

                //Mimic the processing of the event organization e.g. booking venues, ordering services,
                //coordination with vendors (queries to third-party api)
                sleep(10);
                $this->sendMessageToChat($chatId, "Ваш запрос обработан.");

            }
            $msg->ack();
        };

        $this->channel->basic_qos(null, 1, null);
        $this->channel->basic_consume($this->queue, '', false, false, false, false, $callback);

        while ($this->channel->is_consuming()) {
            $this->channel->wait();
        }
    }

    private function sendMessageToChat(int $chatId, string $message)
    {
        Request::sendMessage([
            'chat_id' => $chatId,
            'text' => $message
        ]);
    }

    public function close()
    {
        $this->channel->close();
        $this->connection->close();
    }

    public function __destruct()
    {
        $this->close();
    }
}