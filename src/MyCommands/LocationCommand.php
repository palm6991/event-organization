<?php

namespace Events\Daniel\MyCommands;

use Events\Daniel\Factory\ContainerFactory;
use Events\Daniel\Repository\LocationRepository;
use Longman\TelegramBot\Commands\UserCommand;
use Longman\TelegramBot\Entities\Update;
use Longman\TelegramBot\Request;
use Longman\TelegramBot\Entities\ServerResponse;
use Longman\TelegramBot\Entities\InlineKeyboard;
use Longman\TelegramBot\Telegram;

class LocationCommand extends UserCommand
{
    protected $name = 'location';
    protected $description = 'Choose a location for the event';
    protected $usage = '/location';
    protected $version = '1.0.0';

    private $locationRepository;

    public function __construct(Telegram $telegram, ?Update $update = null)
    {
        parent::__construct($telegram, $update);

        $this->locationRepository = ContainerFactory::create()->get(LocationRepository::class);
    }

    public function execute(): ServerResponse
    {
        $message = $this->getMessage();

        if ($message !== null) {
            $chat_id = $message->getChat()->getId();
        } else {
            $callback_query = $this->getCallbackQuery();
            $chat_id = $callback_query?->getMessage()->getChat()->getId();
        }

        if ($chat_id === null) {
            throw new \Exception("Chat ID could not be determined.");
        }

        $locations = $this->locationRepository->getAvailableLocations();

        $inline_keyboard = new InlineKeyboard([]);
        foreach ($locations as $location) {
            $inline_keyboard->addRow([
                'text' => $location['name'],
                'callback_data' => 'location_' . $location['location_id']
            ]);
        }

        $data = [
            'chat_id' => $chat_id,
            'text' => 'Выберите локацию для вашего мероприятия:',
            'reply_markup' => $inline_keyboard
        ];

        return Request::sendMessage($data);
    }
}