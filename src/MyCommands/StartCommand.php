<?php

namespace Events\Daniel\MyCommands;

use Events\Daniel\Exception\ChatException;
use Longman\TelegramBot\Commands\UserCommand;
use Longman\TelegramBot\Request;
use Longman\TelegramBot\Entities\ServerResponse;
use PDO;

class StartCommand extends UserCommand
{
    protected $name = 'start';
    protected $description = 'Start command';
    protected $usage = '/start';
    protected $version = '1.0.0';

    /**
     * @throws ChatException
     */
    public function execute(): ServerResponse
    {
        try {
            $message = $this->getMessage();
            $chat_id = $message->getChat()->getId();

            $data = [
                'chat_id' => $chat_id,
                'text' => 'Привет! Я помогу вам организовать ваше мероприятие. Какое мероприятие вы хотели бы организовать?',
                'reply_markup' => json_encode([
                    'inline_keyboard' => [
                        [
                            ['text' => 'Продолжить', 'callback_data' => 'eventtype']
                        ]
                    ]
                ])
            ];

            return Request::sendMessage($data);
        } catch (\Exception $e) {
            throw new ChatException($e->getMessage(), $chat_id);
        }

    }
}