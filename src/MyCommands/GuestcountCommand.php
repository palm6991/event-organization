<?php

namespace Events\Daniel\MyCommands;

use Longman\TelegramBot\Commands\UserCommand;
use Longman\TelegramBot\Entities\InlineKeyboard;
use Longman\TelegramBot\Request;
use Longman\TelegramBot\Entities\ServerResponse;

class GuestcountCommand extends UserCommand
{
    protected $name = 'guestcount';
    protected $description = 'Enter guest count';
    protected $usage = '/guestcount';
    protected $version = '1.0.0';

    public function execute(): ServerResponse
    {
        try {
            $message = $this->getMessage();
            $chat_id = null;

            if ($message !== null) {
                $chat_id = $message->getChat()->getId();
            } else {
                $callback_query = $this->getCallbackQuery();
                if ($callback_query) {
                    $chat_id = $callback_query->getMessage()->getChat()->getId();
                }
            }

            if ($chat_id === null) {
                throw new \Exception("Chat ID could not be determined.");
            }

            $inline_keyboard = new InlineKeyboard([
                ['text' => '10', 'callback_data' => 'guestcount_10'],
                ['text' => '20', 'callback_data' => 'guestcount_20'],
                ['text' => '30', 'callback_data' => 'guestcount_30'],
                ['text' => '40', 'callback_data' => 'guestcount_40']
            ]);

            $data = [
                'chat_id' => $chat_id,
                'text'    => 'Выберите количество гостей на мероприятии:',
                'reply_markup' => $inline_keyboard
            ];

            return Request::sendMessage($data);
        } catch (\Exception $e) {
            return $this->replyToChat("Error: " . $e->getMessage());
        }
    }
}