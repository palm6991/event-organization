<?php

namespace Events\Daniel\MyCommands;

use Events\Daniel\Factory\ContainerFactory;
use Events\Daniel\Repository\CateringRepository;
use Exception;
use Longman\TelegramBot\Commands\UserCommand;
use Longman\TelegramBot\Entities\Update;
use Longman\TelegramBot\Exception\TelegramException;
use Longman\TelegramBot\Request;
use Longman\TelegramBot\Entities\ServerResponse;
use Longman\TelegramBot\Entities\InlineKeyboard;
use Longman\TelegramBot\Telegram;

class CateringCommand extends UserCommand
{
    protected $name = 'catering';
    protected $description = 'Choose a catering service';
    protected $usage = '/catering';
    protected $version = '1.0.0';

    private $cateringRepository;

    public function __construct(Telegram $telegram, ?Update $update = null)
    {
        parent::__construct($telegram, $update);

        $this->cateringRepository = ContainerFactory::create()->get(CateringRepository::class);
    }

    /**
     * @throws TelegramException
     * @throws Exception
     */
    public function execute(): ServerResponse
    {
        $message = $this->getMessage();

        if ($message !== null) {
            $chat_id = $message->getChat()->getId();
        } else {
            $callback_query = $this->getCallbackQuery();
            $chat_id = $callback_query?->getMessage()->getChat()->getId();
        }

        if ($chat_id === null) {
            throw new Exception("Chat ID could not be determined.");
        }

        $cateringOptions = $this->cateringRepository->getCateringOptions();

        $inline_keyboard = new InlineKeyboard([]);
        foreach ($cateringOptions as $option) {
            $inline_keyboard->addRow([
                'text'          => $option['provider_name'] . " - $" . $option['price'],
                'callback_data' => 'catering_' . $option['provider_name'] . " - $" . $option['price']
            ]);
        }

        $data = [
            'chat_id'      => $chat_id,
            'text'         => 'Выберите кейтеринг для вашего мероприятия:',
            'reply_markup' => $inline_keyboard
        ];

        return Request::sendMessage($data);
    }
}