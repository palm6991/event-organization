<?php

namespace Events\Daniel\MyCommands;

use Exception;
use Longman\TelegramBot\Commands\UserCommand;
use Longman\TelegramBot\Exception\TelegramException;
use Longman\TelegramBot\Request;
use Longman\TelegramBot\Entities\ServerResponse;

class EventdateCommand extends UserCommand
{
    protected $name = 'eventdate';
    protected $description = 'Enter event date';
    protected $usage = '/eventdate';
    protected $version = '1.0.0';

    /**
     * @throws TelegramException
     * @throws Exception
     */
    public function execute(): ServerResponse
    {
        $message = $this->getMessage();
        $chat_id = null;

        if ($message !== null) {
            $chat_id = $message->getChat()->getId();
        } else {
            // If $message is null, try getting chat_id from CallbackQuery
            $callback_query = $this->getCallbackQuery();
            if ($callback_query) {
                $chat_id = $callback_query->getMessage()->getChat()->getId();
            }
        }

        if ($chat_id === null) {
            throw new Exception("Chat ID could not be determined.");
        }

        // Отправка сообщения для ввода даты мероприятия
        $data = [
            'chat_id' => $chat_id,
            'text'    => 'Выберите дату мероприятия:',
            'reply_markup' => json_encode([
                'inline_keyboard' => [
                    [
                        ['text' => 'Сегодня', 'callback_data' => 'date_today'],
                        ['text' => 'Завтра', 'callback_data' => 'date_tomorrow']
                    ],
                    [
                        ['text' => 'На следующей неделе', 'callback_data' => 'date_next_week']
                    ],
                    [
                        ['text' => 'Выбрать из календаря', 'callback_data' => 'date_calendar']
                    ]
                ]
            ])
        ];

        return Request::sendMessage($data);
    }
}