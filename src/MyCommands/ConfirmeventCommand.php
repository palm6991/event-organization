<?php

namespace Events\Daniel\MyCommands;

use Events\Daniel\Factory\ContainerFactory;
use Events\Daniel\Repository\UserStateRepository;
use Longman\TelegramBot\Commands\UserCommand;
use Longman\TelegramBot\Entities\Update;
use Longman\TelegramBot\Exception\TelegramException;
use Longman\TelegramBot\Request;
use Longman\TelegramBot\Entities\ServerResponse;
use Longman\TelegramBot\Telegram;

class ConfirmeventCommand extends UserCommand
{
    protected $name = 'confirmevent';
    protected $description = 'Confirm the event details';
    protected $usage = '/confirmevent';
    protected $version = '1.0.0';

    private UserStateRepository $userStateRepository;

    public function __construct(Telegram $telegram, ?Update $update = null)
    {
        parent::__construct($telegram, $update);

        $this->userStateRepository = ContainerFactory::create()->get(UserStateRepository::class);
    }

    /**
     * @throws TelegramException
     */
    public function execute(): ServerResponse
    {
        $message = $this->getMessage();
        $chat_id = null;
        $user_id = null;

        if ($message !== null) {
            $chat_id = $message->getChat()->getId();
            $user_id = $message->getFrom()->getId();
        } else {

            $callback_query = $this->getCallbackQuery();
            if ($callback_query) {
                $chat_id = $callback_query->getMessage()->getChat()->getId();
                $user_id = $callback_query->getFrom()->getId();
            }
        }

        if ($chat_id === null) {
            throw new \Exception("Chat ID could not be determined.");
        }

        $eventDetails = $this->userStateRepository->getEventDetails($user_id);

        $detailsMessage = "Подтвердите детали вашего мероприятия:\n";
        $detailsMessage .= "Тип: " . $eventDetails['event_type'] . "\n";
        $detailsMessage .= "Дата: " . $eventDetails['event_date'] . "\n";
        $detailsMessage .= "Количество гостей: " . $eventDetails['guest_count'] . "\n";
        $detailsMessage .= "Локация: " . $eventDetails['location_name'] . "\n";
        $detailsMessage .= "Кейтеринг: " . $eventDetails['catering_name']. "\n";

        $data = [
            'chat_id' => $chat_id,
            'text'    => $detailsMessage,
            'reply_markup' => json_encode([
                'inline_keyboard' => [
                    [['text' => 'Подтвердить', 'callback_data' => 'confirm_yes']],
                    [['text' => 'Изменить', 'callback_data' => 'confirm_no']]
                ]
            ])
        ];

        return Request::sendMessage($data);
    }
}