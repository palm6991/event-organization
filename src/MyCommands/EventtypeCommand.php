<?php

namespace Events\Daniel\MyCommands;

use Events\Daniel\Factory\ContainerFactory;
use Events\Daniel\Repository\EventTypeRepository;
use Longman\TelegramBot\Commands\UserCommand;
use Longman\TelegramBot\Entities\InlineKeyboard;
use Longman\TelegramBot\Entities\InlineKeyboardButton;
use Longman\TelegramBot\Entities\Update;
use Longman\TelegramBot\Request;
use Longman\TelegramBot\Entities\ServerResponse;
use Longman\TelegramBot\Telegram;

class EventtypeCommand extends UserCommand
{
    protected $name = 'eventtype';
    protected $description = 'Choose event type';
    protected $usage = '/eventtype';
    protected $version = '1.0.0';

    private $eventTypeRepository;

    public function __construct(Telegram $telegram, ?Update $update = null)
    {
        parent::__construct($telegram, $update);

        $this->eventTypeRepository = ContainerFactory::create()->get(EventTypeRepository::class);
    }

    public function execute(): ServerResponse
    {
        try {
            $message = $this->getMessage();
            $chat_id = null;

            if ($message !== null) {
                $chat_id = $message->getChat()->getId();
            } else {
                $callback_query = $this->getCallbackQuery();
                if ($callback_query) {
                    $chat_id = $callback_query->getMessage()->getChat()->getId();
                }
            }

            if ($chat_id === null) {
                throw new \Exception("Chat ID could not be determined.");
            }

            $eventTypes = $this->eventTypeRepository->getEventTypes();

            $inline_keyboard = new InlineKeyboard([]);
            foreach ($eventTypes as $eventType) {
                $inline_keyboard->addRow(new InlineKeyboardButton([
                    'text' => $eventType, 'callback_data' => strtolower($eventType)
                ]));
            }

            $data = [
                'chat_id' => $chat_id,
                'text'    => 'Выберите тип мероприятия:',
                'reply_markup' => json_encode($inline_keyboard)
            ];

            return Request::sendMessage($data);
        } catch (\Exception $e) {
            return $this->replyToChat("Error: " . $e->getMessage());
        }
    }
}