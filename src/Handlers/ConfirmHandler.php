<?php

namespace Events\Daniel\Handlers;

use Events\Daniel\Factory\ContainerFactory;
use Events\Daniel\Repository\UserRepository;
use Longman\TelegramBot\Entities\CallbackQuery;
use Longman\TelegramBot\Entities\ServerResponse;
use Longman\TelegramBot\Request;

class ConfirmHandler implements CallbackHandler
{
    private $telegram;
    private $userRepository;

    public function __construct($telegram) {
        $this->telegram = $telegram;
        $this->userRepository = ContainerFactory::create()->get(UserRepository::class);
    }

    public function handle(CallbackQuery $message, string $event_type, string $callback_query_id, string $chat_id): ServerResponse
    {
        $user_id = $message->getFrom()->getId();

        $userName = $message->getFrom()->getUsername();

        $chosen_date = $event_type === 'date_today' ? date('Y-m-d') : date('Y-m-d', strtotime('+1 day'));

        $this->userRepository->updateUserState($user_id, 'eventdate', $chosen_date, $userName);

        Request::answerCallbackQuery([
            'callback_query_id' => $callback_query_id,
            'text' => "Дата выбрана : $event_type. Теперь подтвердите ваш заказ.",
            'show_alert' => true
        ]);

        return $this->telegram->executeCommand('confirmevent');
    }
}