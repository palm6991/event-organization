<?php

namespace Events\Daniel\Handlers;

use Events\Daniel\Factory\ContainerFactory;
use Events\Daniel\Repository\UserRepository;
use Longman\TelegramBot\Entities\CallbackQuery;
use Longman\TelegramBot\Entities\ServerResponse;
use Longman\TelegramBot\Request;

class EventDateHandler implements CallbackHandler
{
    private $telegram;

    private $userRepository;

    public function __construct($telegram) {
        $this->telegram = $telegram;
        $this->userRepository = ContainerFactory::create()->get(UserRepository::class);
    }

    public function handle(CallbackQuery $message, string $event_type, string $callback_query_id, string $chat_id): ServerResponse
    {
        $user_id = $message->getFrom()->getId();

        $userName = $message->getFrom()->getUsername();

        $guest_count  = (int)explode('_', $event_type)[1];

        $this->userRepository->updateUserState($user_id, 'guestcount', $guest_count, $userName);

        Request::answerCallbackQuery([
            'callback_query_id' => $callback_query_id,
            'text' => "Дата выбрана: $event_type. Теперь подтвердите заказ.",
            'show_alert' => true
        ]);

        return $this->telegram->executeCommand('eventdate');
    }
}