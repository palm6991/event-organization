<?php

namespace Events\Daniel\Handlers;

use Events\Daniel\Factory\ProducerFactory;
use Exception;
use Longman\TelegramBot\Entities\CallbackQuery;
use Longman\TelegramBot\Entities\ServerResponse;
use Longman\TelegramBot\Request;

class ProcessingRequestHandler implements CallbackHandler
{
    /**
     * @throws Exception
     */
    public function handle(CallbackQuery $message, string $event_type, string $callback_query_id, string $chat_id): ServerResponse
    {
        $producer = ProducerFactory::getProducer();
        $producer->publishChatId($chat_id);

        return Request::answerCallbackQuery([
            'callback_query_id' => $callback_query_id,
            'text'              => 'Your message is being processed.',
            'show_alert'        => false
        ]);
    }
}