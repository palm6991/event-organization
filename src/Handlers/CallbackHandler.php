<?php

namespace Events\Daniel\Handlers;

use Longman\TelegramBot\Entities\CallbackQuery;
use Longman\TelegramBot\Entities\ServerResponse;

interface CallbackHandler {
    public function handle(CallbackQuery $message, string $event_type, string $callback_query_id, string $chat_id): ServerResponse;
}