<?php

namespace Events\Daniel\Handlers;

use Events\Daniel\Factory\ContainerFactory;
use Events\Daniel\Repository\UserRepository;
use Longman\TelegramBot\Entities\CallbackQuery;
use Longman\TelegramBot\Entities\ServerResponse;

class EventTypeHandler implements CallbackHandler {
    private $telegram;

    private $userRepository;

    public function __construct($telegram) {
        $this->telegram = $telegram;
        $this->userRepository = ContainerFactory::create()->get(UserRepository::class);
    }

    public function handle(CallbackQuery $message, $event_type, $callback_query_id, string $chat_id): ServerResponse {

        return $this->telegram->executeCommand('eventtype');
    }
}