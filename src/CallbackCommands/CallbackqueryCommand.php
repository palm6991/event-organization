<?php

namespace Events\Daniel\CallbackCommands;

use Events\Daniel\Factory\HandlerFactory;
use Events\Daniel\Factory\PatternManagerFactory;
use Exception;
use Longman\TelegramBot\Commands\SystemCommand;
use Longman\TelegramBot\Entities\ServerResponse;
use Longman\TelegramBot\Request;

class CallbackqueryCommand extends SystemCommand
{
    protected $name = 'callbackquery';
    protected $description = 'Handle the callback query';
    protected $version = '1.0.0';
    public HandlerFactory $handlerFactory;

    public function __construct($telegram)
    {
        parent::__construct($telegram);

        $this->handlerFactory = HandlerFactory::getFactory($telegram);
    }

    /**
     * @throws Exception
     */
    public function execute(): ServerResponse {
        $callback_query = $this->getCallbackQuery();
        $callback_data = $callback_query->getData();

        // Access the Message object from the callback query
        $message = $callback_query->getMessage();
        if ($message === null) {
            // Handle cases where there is no message object associated with the callback query
            return $this->defaultResponse($callback_query->getId(), 'No message associated with this callback');
        }

        // Retrieve the chat ID from the message
        $chat_id = $message->getChat()->getId();

        $handlerKey = $this->findHandlerKeyForCallbackData($callback_data);

        if ($handlerKey !== null) {
            $handler = $this->handlerFactory->getHandler($handlerKey);
            // Pass the chat ID to the handler if needed
            return $handler->handle($callback_query, $callback_data, $callback_query->getId(), $chat_id);
        }

        return $this->defaultResponse($callback_query->getId());
    }

    private function defaultResponse(string $callback_query_id): ServerResponse
    {
        return Request::answerCallbackQuery([
            'callback_query_id' => $callback_query_id,
            'text' => 'Command not recognized.',
            'show_alert' => false
        ]);
    }

    private function findHandlerKeyForCallbackData($callbackData): ?string
    {
        if (key_exists($callbackData, $this->handlerFactory->getHandlers())) {
            return $callbackData;
        }

        foreach (PatternManagerFactory::getPatterns() as $pattern => $handlerKey) {
            if (preg_match($pattern, $callbackData)) {
                return $handlerKey;
            }
        }
        return null;
    }
}