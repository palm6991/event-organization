<?php

namespace Events\Daniel\Repository;

use Events\Daniel\DB\DatabaseConnection;

class EventTypeRepository
{
    private \PDO $connection;

    public function __construct(DatabaseConnection $connection)
    {
        $this->connection = $connection->getConnection();
    }

    public function getEventTypes(): array
    {
        try {
            $sql = "SELECT DISTINCT event_type FROM Events";

            $stmt = $this->connection->prepare($sql);
            $stmt->execute();

            $eventTypes = [];
            while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
                $eventTypes[] = $row['event_type'];
            }

            return $eventTypes;
        } catch (\PDOException $e) {
            error_log("Ошибка при удалении состояния пользователя: " . $e->getMessage());
            return  [];
        }
    }
}