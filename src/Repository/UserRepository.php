<?php

namespace Events\Daniel\Repository;

use Events\Daniel\DB\DatabaseConnection;
use Events\Daniel\Factory\StrategyFactory;
use Events\Daniel\Strategies\UserUpdate\UserUpdateContext;

class UserRepository
{
    private \PDO $connection;

    public function __construct(DatabaseConnection $connection)
    {
        $this->connection = $connection->getConnection();
    }

    public function updateUserState(int $user_id, string $command, string $data, string $username): bool
    {
        try {
            $userExists = $this->connection->prepare("SELECT 1 FROM Users WHERE user_id = :user_id");
            $userExists->bindParam(':user_id', $user_id, \PDO::PARAM_INT);
            $userExists->execute();

            if ($userExists->fetchColumn() === false) {
                $insertUser = $this->connection->prepare("INSERT INTO Users (user_id, username, email) VALUES (:user_id, :username, :email)");
                $insertUser->bindParam(':user_id', $user_id, \PDO::PARAM_INT);
                $insertUser->bindValue(':username', $username);
                $insertUser->bindValue(':email', $username . '@example.com');
                $insertUser->execute();
            }

            $sql = "SELECT data FROM UserState WHERE user_id = :user_id";
            $stmt = $this->connection->prepare($sql);
            $stmt->bindParam(':user_id', $user_id, \PDO::PARAM_INT);
            $stmt->execute();
            $currentData = $stmt->fetchColumn();

            $decodedData = json_decode($currentData, true) ?? [];
            $context = new UserUpdateContext(StrategyFactory::create($command) ?? null);

            if ($context->getStrategy() !== null) {
                $context->updateUserData($decodedData, $data);
            } else {
                error_log("Unknown command: $command");
                return false;
            }

            $newData = json_encode($decodedData);

            $sql = "INSERT INTO UserState (user_id, current_command, data, last_update) VALUES (:user_id, :command, :data, NOW())
                ON CONFLICT (user_id) DO UPDATE SET current_command = :command, data = :data, last_update = NOW()";
            $stmt = $this->connection->prepare($sql);
            $stmt->bindParam(':user_id', $user_id, \PDO::PARAM_INT);
            $stmt->bindParam(':command', $command);
            $stmt->bindParam(':data', $newData);
            $stmt->execute();

            return true;
        } catch (\PDOException $e) {
            error_log('Ошибка при обновлении состояния пользователя  а это: ' . $e->getMessage());
            return false;
        }
    }
}