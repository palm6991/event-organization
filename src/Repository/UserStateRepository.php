<?php

namespace Events\Daniel\Repository;

use Events\Daniel\DB\DatabaseConnection;

class UserStateRepository
{
    private \PDO $connection;

    public function __construct(DatabaseConnection $connection)
    {
        $this->connection = $connection->getConnection();
    }

    public function getEventDetails(int $user_id): array
    {
        try {
            $sql = "SELECT data FROM UserState WHERE user_id = :user_id";

            $stmt = $this->connection->prepare($sql);
            $stmt->execute(['user_id' => $user_id]);

            $result = $stmt->fetch(\PDO::FETCH_ASSOC);

            if ($result) {
                $eventDetails = json_decode($result['data'], true);

                return [
                    'event_type' => $eventDetails['eventtype'],
                    'event_date' => $eventDetails['eventdate'],
                    'guest_count' => (int) $eventDetails['guestcount'],
                    'location_name' => $eventDetails['location'],
                    'catering_name' => $eventDetails['catering'],
                ];
            }

            return [];
        } catch (\PDOException $e) {
            error_log("Ошибка при удалении состояния пользователя: " . $e->getMessage());
            return  [];
        }
    }
}