<?php

namespace Events\Daniel\Repository;

use Events\Daniel\DB\DatabaseConnection;

class CateringRepository
{
    private \PDO $connection;

    public function __construct(DatabaseConnection $connection)
    {
        $this->connection = $connection->getConnection();
    }

    public function getCateringOptions(): array
    {
        try {
            $sql = "SELECT catering_id, provider_name, price FROM Catering";

            $stmt = $this->connection->prepare($sql);
            $stmt->execute();

            return $stmt->fetchAll(\PDO::FETCH_ASSOC);
        } catch (\PDOException $e) {
            error_log("Ошибка при удалении состояния пользователя: " . $e->getMessage());
            return  [];
        }
    }
}