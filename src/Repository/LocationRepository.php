<?php

namespace Events\Daniel\Repository;

use Events\Daniel\DB\DatabaseConnection;

class LocationRepository
{
    private \PDO $connection;

    public function __construct(DatabaseConnection $connection)
    {
        $this->connection = $connection->getConnection();
    }

    public function getAvailableLocations(): array
    {
        try {
            $sql = "SELECT location_id, name FROM Locations";

            $stmt = $this->connection->prepare($sql);
            $stmt->execute();

            return $stmt->fetchAll(\PDO::FETCH_ASSOC);
        } catch (\Exception $e) {
            return [];
        }
    }
}