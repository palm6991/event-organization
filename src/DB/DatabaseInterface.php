<?php

namespace Events\Daniel\DB;

interface DatabaseInterface
{
    public function getConnection(): \PDO;
}