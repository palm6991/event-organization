<?php

namespace Events\Daniel\DB;

use Dotenv\Dotenv;

class DatabaseConnection implements DatabaseInterface
{
    private $pdo;

    public function __construct()
    {
        $dotenv = Dotenv::createImmutable(dirname(__DIR__, 2));
        $dotenv->load();

        $dsn = 'pgsql:host=' . $_ENV['DB_HOST'] .
            ';port=' . $_ENV['DB_PORT'] .
            ';dbname=' . $_ENV['DB_NAME'] .
            ';options=' . $_ENV['DB_OPTIONS'];

        try {
            $this->pdo = new \PDO($dsn, $_ENV['DB_USER'], $_ENV['DB_PASSWORD']);
            $this->pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            $this->pdo->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_ASSOC);
        } catch (\PDOException $e) {
            die('Connection failed: ' . $e->getMessage());
        }
    }

    public function getConnection(): \PDO
    {
        return $this->pdo;
    }
}