<?php

namespace Events\Daniel;

use Dotenv\Dotenv;
use Events\Daniel\Exception\ChatException;
use Longman\TelegramBot\Request;
use Longman\TelegramBot\Telegram;

class App {
    private $bot_api_key;
    private $bot_username;

    public function __construct($bot_api_key, $bot_username) {

        $this->bot_api_key = $bot_api_key;
        $this->bot_username = $bot_username;
    }

    public function run() {
        try {
            $telegram = new Telegram($this->bot_api_key, $this->bot_username);

            $telegram->addCommandsPath(__DIR__ . '/../src/MyCommands');
            $telegram->addCommandsPath(__DIR__ . '/../src/CallbackCommands');

            $telegram->handle();
        } catch (ChatException $e) {
            $errorMessage = "An error occurred: " . $e->getMessage();
            $chat_id = $e->getChatId();

            Request::sendMessage([
                'chat_id' => $chat_id,
                'text'    => $errorMessage
            ]);
        }
    }
}