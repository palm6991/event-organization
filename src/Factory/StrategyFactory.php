<?php

namespace Events\Daniel\Factory;

use Events\Daniel\Strategies\UserUpdate\EventDateStrategy;
use Events\Daniel\Strategies\UserUpdate\GuestCountStrategy;
use Events\Daniel\Strategies\UserUpdate\UpdateCateringStrategy;
use Events\Daniel\Strategies\UserUpdate\UpdateEventTypeStrategy;
use Events\Daniel\Strategies\UserUpdate\UpdateLocationStrategy;
use Events\Daniel\Strategies\UserUpdate\UserUpdateStrategyInterface;

class StrategyFactory {
    public static function create(string $command): ?UserUpdateStrategyInterface {
        $strategies = [
            'eventtype'  => UpdateEventTypeStrategy::class,
            'location'   => UpdateLocationStrategy::class,
            'catering'   => UpdateCateringStrategy::class,
            'eventdate'  => EventDateStrategy::class,
            'guestcount' => GuestCountStrategy::class,
        ];

        if (array_key_exists($command, $strategies)) {
            return new $strategies[$command]();
        }

        return null;
    }
}