<?php

namespace Events\Daniel\Factory;

use DI\ContainerBuilder;
use Exception;

class ContainerFactory
{
    /**
     * @throws Exception
     */
    public static function create()
    {
        $containerBuilder = new ContainerBuilder();

        $containerBuilder->addDefinitions(__DIR__.'/../../config.php');

        return $containerBuilder->build();
    }
}