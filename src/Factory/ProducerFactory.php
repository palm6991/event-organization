<?php

namespace Events\Daniel\Factory;

use Dotenv\Dotenv;
use Events\Daniel\Producer\ConfirmationHandlerProducer;
use Exception;
use PhpAmqpLib\Connection\AMQPStreamConnection;

class ProducerFactory
{
    private static ?ConfirmationHandlerProducer $producer = null;

    /**
     * @throws Exception
     */
    public static function getProducer(): ConfirmationHandlerProducer
    {
        if (!isset($_ENV['AMQP_HOST'])) {
            $dotenv = Dotenv::createImmutable(__DIR__ . '/../../vrr');
            $dotenv->load();
        }

        if (self::$producer === null) {
            self::$producer = new ConfirmationHandlerProducer(
                new AMQPStreamConnection(
                    $_ENV['AMQP_HOST'],
                    $_ENV['AMQP_PORT'],
                    $_ENV['AMQP_USER'],
                    $_ENV['AMQP_PASSWORD']
                )
            );
        }
        return self::$producer;
    }
}