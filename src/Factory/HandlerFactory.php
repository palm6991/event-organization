<?php

namespace Events\Daniel\Factory;

use Events\Daniel\Handlers\CallbackHandler;
use Events\Daniel\Handlers\CateringHandler;
use Events\Daniel\Handlers\ConfirmHandler;
use Events\Daniel\Handlers\EventDateHandler;
use Events\Daniel\Handlers\EventTypeHandler;
use Events\Daniel\Handlers\GuestCountHandler;
use Events\Daniel\Handlers\LocationHandler;
use Events\Daniel\Handlers\ProcessingRequestHandler;
use Exception;

class HandlerFactory
{
    private array $handlers;

    public function __construct($telegram)
    {
        $this->handlers = [

            // Handlers objects
            'eventtype' => new EventTypeHandler($telegram),
            'conference' => new LocationHandler($telegram),
            'birthday' => new LocationHandler($telegram),
            'corporate' => new LocationHandler($telegram),
            'meeting' => new LocationHandler($telegram),
            'seminar' => new LocationHandler($telegram),
            'gala' => new LocationHandler($telegram),
            'reception' => new LocationHandler($telegram),
            'workshop' => new LocationHandler($telegram),
            'exhibition' => new LocationHandler($telegram),
            'wedding' => new LocationHandler($telegram),
            'location' => new CateringHandler($telegram),
            'catering' => new GuestCountHandler($telegram),
            'guestcount' => new EventDateHandler($telegram),
            'date_today' => new ConfirmHandler($telegram),
            'date_tomorrow' => new ConfirmHandler($telegram),

            // Confirmation processing handlers
            'confirm_yes' => new ProcessingRequestHandler(),
        ];
    }

    /**
     * @throws Exception
     */
    public function getHandler($type): CallbackHandler
    {
        if (array_key_exists($type, $this->handlers)) {
            return $this->handlers[$type];
        }
        throw new Exception("Unknown handler type: $type");
    }

    public static function getFactory($telegram): self
    {
        return new self($telegram);
    }

    public function getHandlers(): array
    {
        return $this->handlers;
    }
}