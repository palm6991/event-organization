<?php

namespace Events\Daniel\Factory;

class PatternManagerFactory {
    protected static array $patterns = [
        '/^catering_.+$/' => 'catering',
        '/^location_\d+$/' => 'location',
        '/^guestcount_\d+$/' => 'guestcount',
    ];

    public static function getPatterns(): array
    {
        return self::$patterns;
    }
}