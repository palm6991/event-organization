<?php

namespace Events\Daniel\Strategies\UserUpdate;

interface UserUpdateStrategyInterface {
    public function update(array &$data, string $value): void;
}