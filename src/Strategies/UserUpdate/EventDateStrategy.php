<?php

namespace Events\Daniel\Strategies\UserUpdate;

class EventDateStrategy implements UserUpdateStrategyInterface {
    public function update(array &$data, string $value): void {
        $data['eventdate'] = $value;
    }
}