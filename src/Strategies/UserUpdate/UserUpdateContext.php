<?php

namespace Events\Daniel\Strategies\UserUpdate;

class UserUpdateContext {
    private UserUpdateStrategyInterface $strategy;

    public function __construct(UserUpdateStrategyInterface $strategy) {
        $this->strategy = $strategy;
    }

    public function getStrategy(): UserUpdateStrategyInterface
    {
        return $this->strategy;
    }

    public function updateUserData(array &$data, string $value): void {
        $this->strategy->update($data, $value);
    }
}