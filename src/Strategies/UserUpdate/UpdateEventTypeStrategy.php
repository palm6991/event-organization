<?php

namespace Events\Daniel\Strategies\UserUpdate;

class UpdateEventTypeStrategy implements UserUpdateStrategyInterface {
    public function update(array &$data, string $value): void {
        $data['eventtype'] = $value;
    }
}