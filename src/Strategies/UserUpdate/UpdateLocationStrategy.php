<?php

namespace Events\Daniel\Strategies\UserUpdate;

class UpdateLocationStrategy implements UserUpdateStrategyInterface {
    public function update(array &$data, string $value): void {
        $data['location'] = $value;
    }
}