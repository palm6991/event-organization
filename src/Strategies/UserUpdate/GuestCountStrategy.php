<?php

namespace Events\Daniel\Strategies\UserUpdate;

class GuestCountStrategy implements UserUpdateStrategyInterface {
    public function update(array &$data, string $value): void {
        $data['guestcount'] = $value;
    }
}