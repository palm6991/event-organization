<?php

namespace Events\Daniel\Strategies\UserUpdate;

class UpdateCateringStrategy implements UserUpdateStrategyInterface {
    public function update(array &$data, string $value): void {
        $data['catering'] = $value;
    }
}