<?php

namespace Events\Daniel\Exception;

class ChatException extends \Exception {
    private $chatId;

    public function __construct($message, $chatId, $code = 0, \Exception $previous = null) {
        parent::__construct($message, $code, $previous);
        $this->chatId = $chatId;
    }

    public function getChatId() {
        return $this->chatId;
    }
}