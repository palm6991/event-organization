<?php

require __DIR__ . '/../vendor/autoload.php';

use Dotenv\Dotenv;
use Events\Daniel\App;

$dotenv = Dotenv::createImmutable(dirname(__DIR__));
$dotenv->load();

$app = new App($_ENV['BOT_API_KEY'], $_ENV['BOT_USERNAME']);
$app->run();
